const request = require('request');
const async = require('async');
const fs = require('fs');

const todos = [
  {name: "Todo 1"},
  {name: "Todo 2"},
  {name: "Todo 3"}
];

function httpPost(json, callback) {
  const options = {
    url: "http://127.0.0.1:3000/todo",
    method: "POST",
    json: json
  };
  request(options, (err, res, body) => {
      callback(err, body);
    }
  );
}

async.map(todos, httpPost, (err, res) => {
  if (err) return console.log(err);
  console.log(res.map((x) => x.id));
  fs.writeFile("./test/todos.csv", res.map(x => x.id).join("\n"), error => {
    if(error) {
      return console.log(error);
    }

    console.log("The file was saved!");
  });
});