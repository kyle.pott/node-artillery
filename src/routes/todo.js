const express = require('express');
const router = express.Router();

let todoIdCounter = 0;

const todos = [];

router.get('/', function (req, res) {
  res.send(todos);
});

router.get('/:id', function (req, res) {
  res.send(todos.find(x => x.id == req.params.id));
});

router.post('/', function (req, res) {
  const newTodo = {id: getNextId(), name: req.body.name};
  todos.push(newTodo);
  res.send(newTodo);
});

function getNextId() {
  todoIdCounter += 1;
  return todoIdCounter;
}

module.exports = router;
